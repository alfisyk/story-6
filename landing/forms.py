from django import forms
from django.forms import widgets
from .models import StatusModel

class StatusForm(forms.Form):
	status = forms.CharField(
    label='status',
    widget = forms.Textarea(attrs = {'class' : 'form-control','cols': 7, 'rows': 7, 'placeholder': 'Tulis Status Kamu (maksimal 300 karakter)'})
    )
class meta:
		model = StatusModel
		fields = ('status',)