from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusModel

# Create your views here.
def index(request):
    if request.method=='POST':
        form= StatusForm(request.POST)
        if form.is_valid():
            masuk= StatusModel(
                status= form.data['status'],
            )
            masuk.save()
            return redirect('/')

    else:
        form= StatusForm()

    argument= {
        'form_status': form,
        'data': StatusModel.objects.all().values(),
    }

    return render(request,'index.html', argument)
