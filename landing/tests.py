from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import index
from .forms import StatusForm
from .models import StatusModel

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class LandingPageTest(TestCase):
    def test_apakah_ada_url_landing_page(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_tidak_ada_url_landing_page(self):
        response= Client().get('/no_url')
        self.assertEqual(response.status_code, 404)

    def test_apakah_ada_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, Apa Kabar?')

    def test_fungsi_landing_page(self):
        found= resolve('/')
        self.assertEqual(found.func, index)

    def test_buat_object_baru(self):
        post = StatusModel.objects.create(status='example')
        post.save()
        jumlah= StatusModel.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_isi_form_valid(self):
        form_status = {'status': 'Test Doang'}
        form = StatusForm(data=form_status)
        self.assertTrue(form.is_valid())
    
    def test_ada_button_kirim(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('button', content)
        self.assertIn('Kirim', content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_sel_fill_form(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)

        msg = self.browser.find_element_by_id('id_status')
        msg.send_keys("Coba Coba")
        
        submit = self.browser.find_element_by_id('tombol')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn("Coba Coba", self.browser.page_source)

    


    